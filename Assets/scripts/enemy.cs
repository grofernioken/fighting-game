﻿using UnityEngine;
using System.Collections;

public class enemy : MonoBehaviour {

	public Transform target;
	public Transform self;
	private float targetDistance;
	private float attackTarget;
	[SerializeField]
	private float movementSpeed;



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		targetDistance = Vector2.Distance (self.transform.position, target.transform.position);
		if (targetDistance <= 15f) {
			transform.Translate (Vector2.MoveTowards(self.transform.position, target.transform.position, targetDistance) * movementSpeed * Time.deltaTime);
		}
	}
}