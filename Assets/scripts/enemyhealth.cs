﻿using UnityEngine;
using System.Collections;

public class enemyhealth : MonoBehaviour {
	
	public float maxHealth = 100f;
	public float currentHealth = 0f;
	public GameObject healthBar;
	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;
		InvokeRepeating ("decreasehealth", 1f, 1f);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void decreasehealth() {
		currentHealth -= 2f;
		float calculatedHealth = currentHealth / maxHealth;
		SetHealthBar (calculatedHealth);
	}
	
	public void SetHealthBar(float enemyHealth) {
		healthBar.transform.localScale = new Vector3 (enemyHealth, healthBar.transform.localScale.y, healthBar.transform.localScale.z);
	}
}