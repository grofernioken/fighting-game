﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour {

	private Rigidbody2D rigidbody;
	private Animator animator;
	[SerializeField]
	private float movementSpeed;
	private bool attack;
	private bool facingRight;
	[SerializeField]
	private Transform[] groundpoints;
	[SerializeField]
	private float groundRadius;
	[SerializeField]
	private LayerMask whatIsGround;
	private bool isGrounded;
	private bool jump;
	[SerializeField]
	private float jumpForce;
	// Use this for initialization
	void Start () {
		facingRight = true;
		rigidbody = GetComponent<Rigidbody2D> ();
		animator = GetComponent<Animator> ();
	}
	void Update() {
		HandleInput ();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float horizontal = Input.GetAxis ("Horizontal");
		isGrounded = IsGrounded ();
		HandleMovement (horizontal);
		Flip (horizontal);
		HandleAttacks ();
		HandleJumps ();
		ResetValues();
	}
	private void HandleMovement(float horizontal) {
		rigidbody.velocity = new Vector2(horizontal * movementSpeed, rigidbody.velocity.y);
		animator.SetFloat ("speed", Mathf.Abs(horizontal));
		if (isGrounded && jump) {
			isGrounded = false;
			rigidbody.AddForce(new Vector2(0, jumpForce));
		}
	}
	private void HandleAttacks() {
		if (attack) {
			animator.SetTrigger("attack");
		}
	}
	private void HandleJumps() {
		if (jump) {
			animator.SetTrigger ("jump");
			animator.SetBool("grounded", false);
		}
	}
	private void HandleInput() {
		if (Input.GetKeyDown (KeyCode.W)) {
			jump = true;
		}
		if (Input.GetKeyDown(KeyCode.I)) {
			attack = true;
		}
	}
	private void Flip(float horizontal) {
		if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight) {
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale = theScale;
		}
	}
	private void ResetValues() {
		attack = false;
		jump = false;
	}
	private bool IsGrounded() {
		if (rigidbody.velocity.y <= 0) {
			foreach (Transform point in groundpoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
				for (int i = 0; i < colliders.Length; i++) {
					if (colliders[i].gameObject != gameObject) {
						animator.SetBool("grounded", true);
						return true;
					}
				}
			}
		}else if (rigidbody.velocity.y >= 0) {
			foreach (Transform point in groundpoints) {
				Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadius, whatIsGround);
				for (int i = 0; i < colliders.Length; i++) {
					if (colliders[i].gameObject != gameObject) {
						animator.SetBool("grounded", true);
						return true;
					}
				}
			}
		}
		return false;
	}
}